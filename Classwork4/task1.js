// document.addEventListener("DOMContentLoaded", function () {
//     let daysOfWeekInput = document.getElementById("daysOfWeek");
//     let buttons = document.querySelectorAll("#myButton");
  
//     let days = ["ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი", "კვირა"];
  
//     buttons.forEach((button, index) => {
//       button.addEventListener("click", function () {
//         daysOfWeekInput.value = days[index];
//         });
//     });
// });

document.addEventListener("DOMContentLoaded", function(){
    let daysOfWeekInput = document.getElementById("daysOfWeek");
    let buttons = document.querySelectorAll("#myButton");

    let days = ["ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი", "კვირა"];

    buttons.forEach((button, index) => {
        button.addEventListener("click", function() {
            daysOfWeekInput.value = days[index];
        });
    });
});