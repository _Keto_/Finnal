function randomNumber(a, b) {
    let n = [];
    for (let i = 0; i < 30; i++) {
        let randomNumber = Math.floor(Math.random() * (b - a + 1)) + a;
        n.push(randomNumber);
    }
    return n;
}

let a = 1;
let b = 100;
let result = randomNumber(a, b);
document.write(result.join(" , "));

