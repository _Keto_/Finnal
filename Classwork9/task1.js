function MoveDown(){
    var sq = document.getElementById("square");
    t = 0;
    var down_slide = setInterval(function(){
        sq.style.top = t + "px";
        t ++ ;
        if(t >= 350){
            clearInterval(down_slide)
        }
    }, 1);
}

function MoveUp() {
    var sq = document.getElementById("square");
    var t = 350;
    var up_slide = setInterval(function() {
        sq.style.top = t + "px";
        t--;
        if (t <= 0) {
            clearInterval(up_slide);
        }
    }, 1);
}

function MoveRight() {
    var sq = document.getElementById("square");
    var t = 0;
    var right_slide = setInterval(function() {
        sq.style.left = t + "px";
        t++;
        if (t >= 450) {
            clearInterval(right_slide);
        }
    }, 1);
}

function MoveLeft() {
    var sq = document.getElementById("square");
    var t = 450;
    var left_slide = setInterval(function() {
        sq.style.left = t + "px";
        t--;
        if (t <= 0) {
            clearInterval(left_slide);
        }
    }, 1);
}