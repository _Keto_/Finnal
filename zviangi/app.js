// დავწეროთ ფუნქცია, რომელიც გამოიტანს m-დან n-მდე ნატურალურ
// რიცხვებს ცალ-ცალკე ხაზზე, m, n პარამეტრებია. გაითვალისწინეთ, რომ
// m შეიძლება მეტი იყოს n-ზე.

// function ack(m, n){
//     if(m > n){
//         a = n
//         n = m
//         m = a
//     }
//     for(let i = m; i < n; i++){
//         document.write(i + '<br>')
//     }
// }

// ack(50, 39)

//************************************************************************************************/

// დავწეროთ ფუნქცია, რომელიც გადაცემული პარამეტრების მიხედვით
// გამოიტანს შესაბამისი რაოდენობის სტრიქონებისა და სვეტების
// ცხრილი, ასევე გადაცემული პარამეტრის მიხედვით შეცვლის ცხრილის
// ყველა მახასიათებელ პარამეტრს სიგანეს, სიმაღლეს, ფონის ფერს,
// საზღვრის ზომას.

// function tableconstructor(m, n, w, h, bc, bs){
//     tb = '<table border = 1 style="width: '+w+'px; height: '+h+'px; background-color: '+bc+'; border: solid '+bs+'px; ">'
//     for(let i = 0; i <= m; i++){
//         tb += "<tr>"
//         for(let j = 0; j <= n; j++){
//             tb += "<td>"
//             tb += j +"."+ i
//             tb += "</td>"
//         }
//         tb += "</tr>"
//     }
//     tb += "</table>"
//     document.write(tb)
// }

// tableconstructor(5, 15, 100, 50, 'salmon', 3)

//************************************************************************************************/

// დავწეროთ ფუნქცია, რომელიც გამოიტანს 3x4 ცხრილს, რომლის
// თითოეულ უჯრაში ჩასმული იქნება შემთხვევითად აღებული ფოტო 20
// ფოტოდან.

// function tableconstructor(){
//     tb = "<table border = '1'>"

//     for(let i = 0; i <= 3; i++){
//         tb += "<tr>"
//         for(let j = 0; j <= 4; j++){
//             let randnum = Math.floor(Math.random() * 4)
//             tb += "<td>"
//             tb += '<img src="/images/'+randnum+'.jpg">'
//             tb += "</td>"
//         }
//         tb += "</tr>"
//     }
//     tb += "</table>"
//     document.write(tb)
// }

// tableconstructor()

//************************************************************************************************/

// შექმენით ფორმა, რომელიც საშუალებას მოგვცემს ავირჩიოთ
// ცხრილისთვის სასურველი რაოდენობის სვეტები და სტრიქონები, ასევე

// ავირჩიოთ სასურველი რაოდენობის ფოტოები, ღილაკზე დაკლიკებისას
// გამოვიტანოთ არჩეული რაოდენობის სვეტებისა და სტრიქონების
// ცხრილი, რომელშიდაც იქნება ჩასმული არჩეული რაოდენობის
// ფოტოები.

// function tableconstructor(){
//     let rows = parseInt(document.getElementById("rows").value)
//     let cols = parseInt(document.getElementById("cols").value)
//     tb = "<table border = '1'>"
//     for(let i = 0; i <= rows; i++){
//         tb += "<tr>"
//         for(let j = 0; j <= cols; j++){
//             let randnum = Math.ceil(Math.random() * 3)
//             tb += "<td>"
//             tb += '<img  style = "width: 50px; height: 50px" src="/images/'+randnum+'.jpg">'
//             tb += "</td>"
//         }
//         tb += "</tr>"
//     }
//     tb += "</table>"
//     document.write(tb)
// }

//************************************************************************************************/

// დაწერეთ ფუნქცია, რომელიც სტრიქონში მოძებნის სტრიქონების მასივში
// ჩაწერილ სიტყვებს და თუ მოიძებნა რომელიმე სიტყვა, სტრიქონში ამ
// სიტყვის შემადგენელ სიმბოლოებს შეცვლის შესაბამისი რაოდენობის „ * “
// სიმბოლოთი, ე.წ. ტექსტების ფილტრაცია, უცენზურო სიტყვების დაფარვა.

// function mishvelet(text){
//     let badWords = ["yes", "bitch", "badgirls", "money"]
//     for(let i=0;i<badWords.length;i++){
//         a=("*".repeat(badWords[i].length))
//         text = text.replaceAll(badWords[i], a)     
//     }
//     console.log(text)
// }
// mishvelet("yes bitch badgirls money")

//************************************************************************************************/

// დაწერეთ ფუნქცია, რომელიც სტრიქონში დატოვებს მხოლოდ
// განსხვავებულ სიმბოლოებს, ხოლო დანარჩენებს წაშლის, დაადგენს
// თითოეული სიმბოლო რა სიხშირით შეგხვდა სტრიქონში.

// function mishvelet(text){
//     alphabet="abcdefghijklmnopqrstuvwxyzABCDEFGHJIKLMNOPQRSTUVWXYZ"
//     for(let i=0; i<alphabet.length;i++){
//         text= text.replaceAll(alphabet[i], "")
//     }
//     console.log(text)
// }
// mishvelet("asdfghjui87tfvbnAAAASSSSDDDFFHGSDKVFYEIJMNXQSK")

// function lab3_task13(str){
//     let count = 0
//     alphabet="abcdefghijklmnopqrstuvwxyzABCDEFGHJIKLMNOPQRSTUVWXYZ"
//     for(let i = 0; i < str.length; i++){
//         for(let j=0;j<alphabet.length;j++){
//             if (str.charAt(i) == alphabet[j]) {
//                 count++
//             }
//         }
//     }
//     console.log(count)    
// }

// lab3_task13("Thisisatestofthiscode")

//************************************************************************************************/
// 1. ააგეთ ქვემოთ მოცემული ფორმა, რიცხვზე დაჭერის შედეგად ტექსტურ
// ველში გამოიტანეთ კვირის შესაბამისი დღე

// function changeday(n){
//     days = ["ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი", "კვირა"]
//     display = document.getElementById("weekdisplay")
//     display.innerHTML= days[n]
// }

//************************************************************************************************/
// 6. დაწერეთ ფუნქცია, რომელიც ღილაკზე დაჭერის შედეგად div თაგში
// გამოიტანს 30 შემთხვევით რიცხვ [a, b] შუალედიდან, a-ს და b-ს შეტანა
// მოახდინეთ ფორმიდან.

// function changeday(n,m){
//     for(let i=0;i<30;i++){
//         k=Math.floor(Math.random()*(m-n+1)+n)
//         display = document.getElementById("weekdisplay")
//         display.innerHTML+= k +" "
//     }
// }
// changeday(5,30)

//************************************************************************************************/
// 7. შექმენით ფორმა, რომელიც საშუალებითაც შესაძლებელია ცხრილის
// სასურველი რაოდენობის სვეტები და სტრიქონების არჩევა, ასევე
// სასურველი რაოდენობის ფოტოების არჩევა, ღილაკზე დაჭერისას div
// თაგში გამოიტანეთ არჩეული რაოდენობის სვეტებისა და სტრიქონების

// ცხრილი, რომელშიდაც იქნება ჩასმული არჩეული რაოდენობის ფოტოები
// (10 ფოტოდან) მონაცემების შეტანის სისწორე შეამოწმეთ პროგრამულად.

// function tableconstructor(){
//     let rows = parseInt(document.getElementById("rows").value)
//     let cols = parseInt(document.getElementById("cols").value)

//     tb = "<table border = '1'>"
//     for(let i = 1; i <= rows; i++){
//         tb += "<tr>"
//         for(let j = 1; j <= cols; j++){
//             let randnum = Math.ceil(Math.random() * 3)
//             tb += "<td>"
//             tb += '<img  style = "width: 50px; height: 50px" src="/images/'+randnum+'.jpg">'
//             tb += "</td>"
//         }
//         tb += "</tr>"
//     }
//     tb += "</table>"
//     document.getElementById("tab").innerHTML=tb
// }


//************************************************************************************************/

// let a = prompt("sfdhujiks")
// switch(a) {
//     case "Coca-Cola":
//       text = "Excellent choice. Coca-Cola is good for your soul.";
//       break;
//     case "Pepsi":
//       text = "Pepsi is my favorite too!";
//       break;
//     case "Sprite":
//       text = "Really? Are you sure the Sprite is your favorite?";
//       break;
//     default:
//       text = "I have never heard of that one..";
//   }
// document.getElementById("tab").innerHTML=text


//************************************************************************************************/
// 5. შექმენით ელექტორნული საათი:

// function time(){
//     h=new Date().getHours()
//     m=new Date().getMinutes()
//     s=new Date().getSeconds()
//     document.getElementById("tab").innerHTML=h+":"+m+":"+s

// }
// setInterval(time,1000)

//************************************************************************************************/
// 7. ქართლი ალკენდრის აგება:

// function geoCalendar(){
//     const WeekdaysList = ["კვირა", "ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი"]
//     const MonthsList = ["იანვარი", "თებერვალი", "მარტი", "აპრილი", "მაისი", "ივნისი", "ივლისი", "აგვისტო", "სექტემბერი", "ოქტომბერი", "ნოემბერი", "დეკემბერი"]
//     const currentTime = new Date()
//     const currentYear = currentTime.getFullYear()
//     const currentMonth = MonthsList[currentTime.getMonth()]
//     const currentDate = currentTime.getDate()
//     const currentDay = WeekdaysList[currentTime.getDay()]
//     const currentDateElement = document.getElementById("tab")
//     currentDateElement.textContent = currentDay + ", " + currentDate + " " + currentMonth + ", " + currentYear + " წელი"
// }

// geoCalendar()

//************************************************************************************************/
// 1. ააგეთ ქვემოთ მოყვანილი ფორმა და ღილაკებზე დაჭერის შესაბამისად
// მოახდინეთ კვადრატის მოძრაობის დემონსტრირება შესაბამისი
// მიმართულებებით.

// function MoveDown(){
//     let sq = document.getElementById("sq");
//     t = 0
//     let down_slide = setInterval(function(){
//         sq.style.top = t + "px"
//         t++
//         if(t>=270){
//             clearInterval(down_slide)
//         }
//     }, 1)
  
// }

// function MoveUp(){
//     let sq = document.getElementById("sq");
//     t = 270
//     let up_slide = setInterval(function(){
//         sq.style.top = t + "px"
//         t--
//         if(t<=0){
//             clearInterval(up_slide)
//         }
//     }, 1)
  
// }

// function MoveRight(){
//     let sq = document.getElementById("sq");
//     t = 470
//     let right_slide = setInterval(function(){
//         sq.style.left = t + "px"
//         t--
//         if(t<=0){
//             clearInterval(right_slide)
//         }
//     }, 1)
  
// }

// function MoveLeft(){
//     let sq = document.getElementById("sq");
//     t = 0
//     let left_slide = setInterval(function(){
//         sq.style.left = t + "px"
//         t++
//         if(t>=470){
//             clearInterval(left_slide)
//         }
//     }, 1)
  
// }

//************************************************************************************************/

// მარტივი სლაიდერი JavaScript-ზე, ფოტოზე მიტანისას SLIDER ველში ხდება
// შესაბამისი ფოტოს ჩატვირთვა:

// i=1
// function left(){
//     i-- 
//     if(i<0){
//         i=3
//     }
// }
// function right(){
//     i++
//     if(i>3){
//         i=1
//     }
// }

// function slides(){
//     a=document.getElementById("pic")
//     a.innerHTML='<img src="images/'+i+'.jpg" style="width:300px;">'
// }
// setInterval(slides,1)


// //************************************************************************************************/
// დავალება 6
// დაწერეთ ფუნქცია, რომელიც გადაცემული N (N&lt;=10) პარამეტრის მიხედვით ეკრანზე დაბეჭდავს N რაოდენობის განსხვავებული
// ფერის კვადრატს. ფერების შეცვლა მოახდინეთ ყოველ ერთ წამში ერთხელ.

// function randcolor(n){

//     a=document.getElementById("pic")
//     for(let i=0;i<n;i++){
//         r= Math.floor(Math.random()*256)
//         g= Math.floor(Math.random()*256)
//         b= Math.floor(Math.random()*256)
//         a.innerHTML+='<div style="width:30px;height:30px; background-color: rgb('+r+', '+g+', '+b+');display: inline-block;"></div>'
//     }
// }
// setInterval(randcolor,200,5)

// //************************************************************************************************/

// 4. შეიტანეთ ინგლისური ანბანის სიმბოლოები და გამოიტანეთ მათი
// ქართული ანბანის შესაბამისი სიმბოლო შეტანის პროცესში და პირიქით.

// textinput = document.getElementsById("textinput").value

function mishvelet(e){
    let a = document.getElementById("textoutput")
    if(e.keyCode >= 97 && e.keyCode <= 122){
        a.innerHTML += String.fromCharCode(e.keyCode + 4207)
    }
    console.log(e)
}