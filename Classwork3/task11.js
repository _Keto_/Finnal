function oneCharacters(inputString) {
  let t = {};
  for (let char of inputString) {
    t[char] = (t[char] || 0) + 1;
  }
  return t;
}

let s = "hello my name is keti";
let result = oneCharacters(s);
console.log(result);